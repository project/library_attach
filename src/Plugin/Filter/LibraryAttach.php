<?php

namespace Drupal\library_attach\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\CssSelector\CssSelectorConverter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to attach libraries based on looking for HTML selectors.
 *
 * @Filter(
 *   id = "library_attach",
 *   title = @Translation("Library scanner"),
 *   description = @Translation("Scans HTML for relevant libraries to automatically include in the current page."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class LibraryAttach extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme manager service.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * An array of selectors keyed by library name (including extension).
   *
   * @var string[]
   */
  protected $librarySelectors;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->libraryDiscovery = $container->get('library.discovery');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->themeManager = $container->get('theme.manager');
    $instance->cache = $container->get('cache.data');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    if ($library_selectors = $this->getLibrarySelectors()) {
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);

      foreach ($library_selectors as $library => $selector) {
        if ($xpath->query($selector)->count()) {
          $result->addAttachments(['library' => [$library]]);
        }
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $tips = $this->t('Scans HTML for relevant libraries to automatically include in the current page.');
    if ($long && $libraries = $this->getLibrarySelectors()) {
      $tips .= ' ' . $this->t('The following libraries are supported: @libraries', ['@libraries' => implode(', ', array_keys($libraries))]);
    }
    return $tips;
  }

  /**
   * Get the library selectors.
   *
   * @return string[]
   *   An array of selectors keyed by library name (including extension).
   */
  public function getLibrarySelectors() {
    if (!isset($this->librarySelectors)) {
      if ($cache = $this->cache->get(__METHOD__)) {
        $this->librarySelectors = $cache->data;
      }
      else {
        $this->librarySelectors = [];

        $converter = new CssSelectorConverter();
        $modules = array_keys($this->moduleHandler->getModuleList());
        $active_theme = $this->themeManager->getActiveTheme()->getName();
        $extensions = array_merge(['core'], $modules, [$active_theme]);

        foreach ($extensions as $extension) {
          $libraries = $this->libraryDiscovery->getLibrariesByExtension($extension);
          foreach ($libraries as $library_name => $library) {
            if (!empty($library['filter-selector-xpath'])) {
              $this->librarySelectors[$extension . '/' . $library_name] = $library['filter-selector-xpath'];
            }
            elseif (!empty($library['filter-selector-css'])) {
              $this->librarySelectors[$extension . '/' . $library_name] = $converter->toXPath($library['filter-selector-css']);
            }
          }
        }
        // Cache our data with the 'library_info' cache tag which is used by
        // \Drupal\Core\Asset\LibraryDiscoveryCollector::__construct()
        $this->cache->set(__METHOD__, $this->librarySelectors, Cache::PERMANENT, ['library_info']);
      }
    }

    return $this->librarySelectors;
  }

}
