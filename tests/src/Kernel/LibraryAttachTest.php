<?php

namespace Drupal\Tests\library_attach\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\library_attach\Plugin\Filter\LibraryAttach;

/**
 * Tests for the LibraryAttach filter plugin.
 *
 * @group library_attach
 */
class LibraryAttachTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'library_attach',
    'library_attach_test',
  ];

  /**
   * The LibraryAttach filter plugin.
   *
   * @var \Drupal\library_attach\Plugin\Filter\LibraryAttach
   */
  protected $sut;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->sut = LibraryAttach::create(
      $this->container,
      [],
      'library_attach',
      ['provider' => 'library_attach']
    );
  }

  /**
   * Test no libraries are attached for a table without an expected selector.
   */
  public function testNoLibrariesAttached() {
    $text = <<<EOD
<table class="foo">
	<tbody>
		<tr>
			<td>Foo</td>
		</tr>
		<tr>
			<td>1</td>
			<td>2</td>
		</tr>
	</tbody>
</table>
EOD;

    $result = $this->sut->process($text, 'en');
    $this->assertEmpty($result->getAttachments());
  }

  /**
   * Test table variations include library.
   *
   * @dataProvider elementProvider
   */
  public function testElementVariations($element, $library) {
    $text = $element;
    $result = $this->sut->process($text, 'en');
    $attachments = $result->getAttachments();
    $this->assertArrayHasKey('library', $attachments);
    $this->assertTrue(in_array($library, $attachments['library']));
  }

  /**
   * Data provider for table selector variations.
   *
   * @return array
   *   Array of string markup for a table element.
   */
  public function elementProvider() {
    return [
      [
        '<table class="test1"><tbody><td>Foo</td></tbody></table>',
        'library_attach_test/test-css',
      ],
      [
        '<table class="test1 test2"><tbody><td>Foo</td></tbody></table>',
        'library_attach_test/test-css',
      ],
      [
        '<table class="foo test2"><tbody><td>Foo</td></tbody></table>',
        'library_attach_test/test-css',
      ],
      [
        '<div id="foo" class="test3"><p>This is a test.</p></div>',
        'library_attach_test/test-css',
      ],
      [
        '<table class="test4"><tbody><td>Foo</td></tbody></table>',
        'library_attach_test/test-xpath',
      ],
    ];
  }

}
